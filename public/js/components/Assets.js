import React, { Component } from 'react';
import NewAsset from '../containers/NewAsset';
import { 
  loadAssets, 
  deleteAsset, 
  editAsset,
  toggleCreateAssetsForm 
} from '../actions/assets';
import AssetsView from './assets/AssetsView';
import '../../css/assets.css';

class Assets extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // const { initialize, visible, dispatch } = this.props;
    let { dispatch } = this.props;
    console.log('ASSETS container...', this.props);
    dispatch(loadAssets());
  }

  create(e) {
    e.preventDefault();
  }

  toggleCreateForm(e) {
    e.preventDefault();
    let { dispatch } = this.props;
    dispatch(toggleCreateAssetsForm());
    // this.props.toggleCreateForm(this.props.visible);
  }

  onDelete(assetId) {
    console.log('-----DELETE-------', assetId);
    let { dispatch } = this.props;
    dispatch(deleteAsset(assetId));
  }

  onEdit(assetId, assetType) {
    console.log('---------EDIT-------', this.props);
    let { dispatch} = this.props;
    dispatch(editAsset(assetId, assetType));
  }

  render () {
    let visible = this.props.visible;
    let assets = this.props.assets;
    let createBtnTxt = visible ? 'X' :'Create Assets';

    console.log('ASSETS...........', assets)

    let assetsView = assets.map((asset, index) => 
      <AssetsView 
        asset={asset} 
        onDelete={this.onDelete.bind(this)} 
        onEdit={this.onEdit.bind(this)}
        key={index} />
    )

    return (
      <div className='ui container' style={{marginTop: 20}}>
        <div className='ui text menu greyHeader'>
          <div className='item'>
            <img className='ui image smallicon' src='/images/icon_Assets-Summary.png' 
            />
          </div>
          <div className='item sub-section'>Assets Summary ({assets.length})</div>
          
          <div className='ui right item'>
            <button className="ui basic dark button" 
              onClick={this.toggleCreateForm.bind(this)}>
              { createBtnTxt }
            </button>
          </div>
        </div>
        {
          (() => {
            if (visible) {
              return <NewAsset />
            }
            return;
          })()
        }
        
        <div className='ui three stackable cards'>
          { assetsView }
        </div>
      </div>
    );
  }
}

export default Assets;
