import React, { Component } from 'react';
import '../../css/assets.css';

export default class Income extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
   
  }

  create(e) {
    e.preventDefault();
  }

  render () {
    return (
      <div className='ui container' style={{marginTop: 20}}>
        <div className='ui text menu greyHeader'>
          <div className='item'>
            <img className='ui image smallicon' src='/images/icon_Assets-Summary.png' 
            />
          </div>
          <div className='item sub-section'>Income</div>
        </div>
        
        <div className='ui three stackable cards'>
          <div className="ui card">
            <div className="content" style={{margin: '0.8em', padding: 0}}>
              <img className="right floated mini ui image" src='images/icon_delete.png '
                 />
              <img className="right floated mini ui image" src='images/icon_edit.png '
                />
              <div className="header sub-section" style={styles.header}>
                Employment
              </div>
              <div className='ui grid content' style={{padding: 10}}>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Monthly Income
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  $3,800
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Started On
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  15 FEB 17
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Property Address
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  -
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Ended on
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  -
                </div>
              </div>
            </div>
          </div>

          <div className="ui card">
            <div className="content" style={{margin: '0.8em', padding: 0}}>
              <img className="right floated mini ui image" src='images/icon_delete.png '
                 />
              <img className="right floated mini ui image" src='images/icon_edit.png '
                />
              <div className="header sub-section" style={styles.header}>
                Rental Income
              </div>
              <div className='ui grid content' style={{padding: 10}}>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Monthly Income
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  $3,800
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Started On
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  15 FEB 17
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Property Address
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  #82/2, North Wing, CA
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Ended on
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  -
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  header: {
    padding: 10,
    borderBottom: '1px solid #AAA'
  },
  miniPad: {
    paddingBottom: 8,
    paddingTop: 8
  },
  actionIcon: {
    cursor: 'pointer',
    position: 'relative',
    marginRight: 5
  },
  verticalDivider: {
    color: '#B00414'
  }
}
