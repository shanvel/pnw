import React, { Component } from 'react';
import { showNewAssetForm } from '../actions/newasset';
import {
  SHOW_CREATE_BANK_ASSET_TYPE,
  SHOW_CREATE_MOTOR_VEHICLE_TYPE,
  saveNewAsset
} from '../actions/newasset';

import NewBankForm from './assets/NewBankForm';
import NewMotorForm from './assets/NewMotorForm';

export default class NewAsset extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.instantiateUIComponents();
  }

  instantiateUIComponents() {
    // Initiate Semantic UI Dropdown
    $('.ui.dropdown').dropdown();
    $('#as_at_date').calendar({
			type: 'date'
		});
    $('.ui.radio.checkbox').checkbox();
  }

  saveAssets(e) {
    let { dispatch } = this.props;
    let $form = $('#myForm');
    let allValues = $form.form('get values');
    console.log('ONSUBMIT', allValues);
    dispatch(saveNewAsset(allValues));
  }

  onChangeAssetType(e) {
    const selectedAssetType = e.target.value;
    let { dispatch } = this.props;
    dispatch(showNewAssetForm(selectedAssetType));
  }

  getAssetTypeComponent(assetType) {
    switch (assetType) {
      case SHOW_CREATE_BANK_ASSET_TYPE:
        return <NewBankForm onSave={this.saveAssets.bind(this)} />;
      case SHOW_CREATE_MOTOR_VEHICLE_TYPE:
        return <NewMotorForm onSave={this.saveAssets.bind(this)} />;
      default:
        return <div>Not in scope for POC.</div>;
    }
  }

  render() {
    let assetType = this.props.root.assetType;
    let showNewAssetType = this.getAssetTypeComponent(assetType);

    return (
      <div className='ui grid container' style={{marginBottom: 20}}>
      <div className="sixteen wide column" style={styles.border}>
        <form id="myForm" className="ui form">
          <div className="ui stackable two column grid">
            <div className='column'>
              <div className='ui form'>
                <div className="inline field">
                  <label className='five wide field'>Asset Type</label>
                  <select className='six wide field' 
                    name="assetType" id="assetType" className="ui dropdown" placeholder="Select" 
                    onChange={this.onChangeAssetType.bind(this)} >
                    <option value=''>Select</option>
                    <option value='bank_account'>Bank Account</option>
                    <option value='investment'>Investment</option>
                    <option value='other_investment'>Investment - Other</option>
                    <option value='motor_vehicle'>Motor Vehicle</option>
                    <option value='recreational_vehicle'>Recreational Vehicle</option>
                    <option value='property'>Property</option>
                    <option value='other_asset'>Other Asset</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='column'></div>
          </div>
          {showNewAssetType}
        </form>
      </div>
    </div>
    )
  }
}

const styles = {
  border: {
    border: '1px solid #DDD'
  }
}
