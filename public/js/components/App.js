import React, { Component } from 'react';
import Header from './Header';
// import SubHeader from './SubHeader';
import SubHeader from '../containers/SubHeader';
import Content from '../containers/Content';

class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    console.log("PROPS...", this.props.todos)
  }

  render() {
    return (
      <div>
        <Header />
        <SubHeader />
        <Content />
      </div>
    )
  }

}

export default App;
