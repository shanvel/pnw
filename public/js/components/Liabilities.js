import React, { Component } from 'react';
import '../../css/assets.css';

export default class Liabilities extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
   
  }

  create(e) {
    e.preventDefault();
  }

  render () {
    return (
      <div className='ui container' style={{marginTop: 20}}>
        <div className='ui text menu greyHeader'>
          <div className='item'>
            <img className='ui image smallicon' src='/images/icon_Assets-Summary.png' 
            />
          </div>
          <div className='item sub-section'>Liabilities</div>
        </div>
        
        <div className='ui three stackable cards'>
          <div className="ui card">
            <div className="content" style={{margin: '0.8em', padding: 0}}>
              <img className="right floated mini ui image" src='images/icon_delete.png '
                 />
              <img className="right floated mini ui image" src='images/icon_edit.png '
                />
              <div className="header sub-section" style={styles.header}>
                Credit Card Details
              </div>
              <div className='ui grid content' style={{padding: 10}}>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Credit Card Detail
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  000920/6515481
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Balance
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  $3800
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Credit Limit
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  $390020
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Monthly Payment
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  -
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Joint Spouse
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  Others
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  As At
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  15 FEB 17
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  CIBC Assets/Other Assets
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  Others
                </div>
              </div>
            </div>
          </div>

          <div className="ui card">
            <div className="content" style={{margin: '0.8em', padding: 0}}>
              <img className="right floated mini ui image" src='images/icon_delete.png '
                 />
              <img className="right floated mini ui image" src='images/icon_edit.png '
                />
              <div className="header sub-section" style={styles.header}>
                Credit Line Detail
              </div>
              <div className='ui grid content' style={{padding: 10}}>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Credit Line Detail
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  Toyota/Corola/2017
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Balance
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  $30,500
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Credit Limit
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  -
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Monthly Payment
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  $1600
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Joint Spouse
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  CIBC
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  As At
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  15 FEB 17
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  CIBC Assets/Other Assets
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  CIBC
                </div>
              </div>
            </div>
          </div>

          <div className="ui card">
            <div className="content" style={{margin: '0.8em', padding: 0}}>
              <img className="right floated mini ui image" src='images/icon_delete.png '
                 />
              <img className="right floated mini ui image" src='images/icon_edit.png '
                />
              <div className="header sub-section" style={styles.header}>
                Loan Detail
              </div>
              <div className='ui grid content' style={{padding: 10}}>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Loan Detail
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  A284-2088-Loughee
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Balance
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  $3,800
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Credit Limit
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  -
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Monthly Payment
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  $800
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  Joint Spouse
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  Others
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  As At
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  15 FEB 17
                </div>
                <div className='ten wide column label light' style={styles.miniPad}>
                  CIBC Assets/Other Assets
                </div>
                <div className='six wide column field dark' style={styles.miniPad}>
                  CIBC
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    );
  }
}

const styles = {
  header: {
    padding: 10,
    borderBottom: '1px solid #AAA'
  },
  miniPad: {
    paddingBottom: 8,
    paddingTop: 8
  },
  actionIcon: {
    cursor: 'pointer',
    position: 'relative',
    marginRight: 5
  },
  verticalDivider: {
    color: '#B00414'
  }
}
