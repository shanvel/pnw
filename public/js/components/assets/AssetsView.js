import React from 'react';
import _string from 'underscore.string';

const AssetsView = ({asset, onDelete, onEdit}) => {
  var assetsView = (function () {
    let arr = [];
    for (var key in asset) {
      let value = asset[key];
      if (key == 'assetType' || key === 'id' || key === 'customerId')
        continue;
      key = _string.titleize(_string.humanize(key));
      arr.push(
        <div className='ten wide column label light' style={styles.miniPad}>
          { key }
        </div>
      )
      arr.push(
        <div className='six wide column field dark' style={styles.miniPad}>
          { _string.capitalize(value) }
        </div>
      )
    }
    return arr;
  }());

  var handleOnDelete = (e) => {
    e.preventDefault();
    onDelete(asset.id);
  }

  var handleOnEdit = (e) => {
    e.preventDefault();
    onEdit(asset.id, asset.assetType);
  }

  return (
    <div className="ui card">
      <div className="content" style={{margin: '0.8em', padding: 0}}>
        <img className="right floated mini ui image" src='images/icon_delete.png '
          onClick={handleOnDelete} style={styles.actionIcon} />
        <img className="right floated mini ui image" src='images/icon_edit.png '
          onClick={handleOnEdit} style={styles.actionIcon}/>
        <div className="header sub-section" style={styles.header}>
          {_string.titleize(_string.humanize(asset.assetType))}
        </div>
        <div className='ui grid content' style={{padding: 10}}>
          { assetsView }
        </div>
      </div>
    </div>
  )
}

const styles = {
  header: {
    padding: 10,
    borderBottom: '1px solid #AAA'
  },
  miniPad: {
    paddingBottom: 8,
    paddingTop: 8
  },
  actionIcon: {
    cursor: 'pointer',
    position: 'relative',
    marginRight: 5
  },
  verticalDivider: {
    color: '#B00414'
  }
}

export default AssetsView;
