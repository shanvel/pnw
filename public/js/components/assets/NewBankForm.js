import React, { Component } from 'react';

export default class NewBankForm extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    $('.ui.radio.checkbox').checkbox();
  }

  onSave(e) {
    e.preventDefault();
    let { onSave } = this.props;
    onSave();
  }

  render() {
    return (
      <div className='ui stackable two column grid'>
        <div className='column'>
          <div className='ui form'>
            <div className='inline field' style={styles.inlineField}>
              <label className='five wide field'>Company Name</label>
              <input className='six wide field' type="text" name="companyName" placeholder="" />
            </div>
            <div className='inline field' style={styles.inlineField}>
              <label className='five wide field'>Account number</label>
              <input className='six wide field' type="text" name="accountNumber" placeholder="" />
            </div>
            <div className='inline field' style={styles.inlineField}>
              <label className='five wide field'>Estimated balance</label>
              <input className='six wide field' type="text" name="estimatedBalance" placeholder="" />
            </div>
            <div className='inline field' style={styles.inlineField}>
              <label className='five wide field'>Transit</label>
              <input className='six wide field' type="text" name="transit" placeholder="" />
            </div>
          </div>
        </div>
        <div className='column'>
          <div className='ui form'>
            <div className='inline fields'>
              <label className='five wide field'>Asset Source</label>
              <div className="field">
                <div className="ui radio checkbox five wide field">
                  <input type="radio" value="CIBC" name="assetSource" checked="" tabIndex="0" className="hidden"/>
                  <label>CIBC</label>
                </div>
              </div>
              <div className="field">
                <div className="ui radio checkbox">
                  <input type="radio" value="CIBC_instrument" name="assetSource" tabIndex="0" className="hidden"/>
                  <label>CIBC Instrument</label>
                </div>
              </div>
            </div>

            <div className='inline fields'>
              <label className='five wide field'>CIBC Instrument</label>
              <div className="field">
                <div className="ui radio checkbox five wide field">
                  <input type="radio" value='yes' name="cibcInstrument" checked="" tabIndex="0" className="hidden"/>
                  <label>CIBC</label>
                </div>
              </div>
              <div className="field">
                <div className="ui radio checkbox">
                  <input type="radio" value='no' name="cibcInstrument" tabIndex="0" className="hidden"/>
                  <label>Other</label>
                </div>
              </div>
            </div>
            <div className='inline fields'>
              <label className='five wide field'>Joint with Spouse</label>
              <div className="field">
                <div className="ui radio checkbox five wide field">
                  <input type="radio" value='yes' name="jointWithSpouse" checked="" tabIndex="0" className="hidden"/>
                  <label>Yes</label>
                </div>
              </div>
              <div className="field">
                <div className="ui radio checkbox">
                  <input type="radio" value='no' name="jointWithSpouse" tabIndex="0" className="hidden"/>
                  <label>No</label>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className='sixteen wide column'>
          <button className="ui red basic button">Reset</button>
          <button className="ui basic dark button" onClick={this.onSave.bind(this)}>Save</button>
        </div>
      </div>
    )
  }
}

let styles = {
  inlineField: {
    marginBottom: 5
  }
}