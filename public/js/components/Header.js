import React, { Component } from 'react';

export default class Header extends Component {
  render() {
    return (
      <div className="ui top segment menu" style={styles.header}>
        <div className='ui container' style={styles.innerContainer}>
          <div className="item" style={styles.removePadding}>
            <img src="/images/logo.png" style={styles.imageWidth}/>
          </div>
          <div className="right menu">
            <button className="ui red basic button">Logout</button>
          </div>
        </div>
      </div>
    )
  }
}

const styles = {
  header: {
    borderTop: '3px solid #B00414',
    boxShadow: 'none',
    borderBottom: 0,
    marginBottom: 0,
    marginTop: 1,
  },
  removePadding: {
    padding: 0,
  },
  imageWidth: {
    width: 50
  },
  innerContainer: {
    border: 0,
  }
}
