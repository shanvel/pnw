import React, { Component } from 'react';
import numeral from 'numeral';
import { fetchSummary } from '../actions/summary';

export default class Summary extends Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    console.log("SUMMARY PROPS", this.props)
    let { dispatch } = this.props;
    dispatch(fetchSummary());
  }

  onChangeSpouseIncome(e) {
    console.log(e.target.value)
    $('#spouseIncome').val(e.target.value);
  }

  render() {
    console.log('-------------', this.props)
    let { 
      totalLiabilities,
      totalPayments,
      principalIncome,
      spouseIncome,
      totalExpense,
    } = this.props.summary;

    let assets = this.props.assets;
    let totalAssets = _.sumBy(assets, 'estimatedBalance');

    totalAssets = numeral(totalAssets).format('$ 0,0[.]00');
    totalLiabilities = numeral(totalLiabilities).format('$ 0,0[.]00');
    totalPayments = numeral(totalPayments).format('$ 0,0[.]00');
    let totalIncome = principalIncome + spouseIncome;
    totalIncome = numeral(totalIncome).format('$ 0,0[.]00');
    principalIncome = numeral(principalIncome).format('$ 0,0[.]00');
    totalExpense = numeral(totalExpense).format('$ 0,0[.]00');

    return (
      <div className='ui container' style={styles.container}>
        <div className='ui text menu greyHeader'>
          <div className='item'>
            <img className='ui image smallicon' src='/images/icon_Personal.png' 
            />
          </div>
          <div className='item sub-section'>Personal Net Worth Summary</div>
        </div>

        <div className='ui seven stackable cards'>
          <div className='ui card'>
            <div className='content' style={{textAlign: 'center'}}>
              <h3>{ totalAssets }</h3>
            </div>
            <div className='ui segment' style={styles.imageAlign}>
              <img className='ui centered medium image' src='/images/icon_Total-Assets.png' style={styles.icon} />
            </div>
            <div className='ui bottom attached buttons'>
              <div className='ui button' style={styles.title}>
                Total <br/> Assets
              </div>
            </div>
          </div>
          <div className='ui card'>
            <div className='content' style={{textAlign: 'center'}}>
              <h3> {totalLiabilities}</h3>
            </div>
            <div className='ui segment' style={styles.imageAlign}>
              <img className='ui centered medium image' src='/images/icon_Total-Liabilities.png' style={styles.icon}/>
            </div>
            <div className='ui bottom attached buttons'>
              <div className='ui button' style={styles.title}>
                Total <br/> Liabilities
              </div>
            </div>
          </div>
          <div className='ui card'>
            <div className='content' style={{textAlign: 'center'}}>
              <h3> {totalPayments}</h3>
            </div>
            <div className='ui segment' style={styles.imageAlign}>
              <img className='ui centered medium image' src='/images/icon_Total-Monthly-Payment.png' style={styles.icon} />
            </div>
            <div className='ui bottom attached buttons'>
              <div className='ui button' style={styles.title}>
                Total <br/> Monthly Payments
              </div>
            </div>
          </div>
          <div className='ui card'>
            <div className='content' style={{textAlign: 'center'}}>
              <h3> {principalIncome}</h3>
            </div>
            <div className='ui segment' style={styles.imageAlign}>
              <img className='ui centered medium image' src='/images/icon_Principals-Monthly-Income.png' style={styles.icon}/>
            </div>
            <div className='ui bottom attached buttons'>
              <div className='ui button' style={styles.title}>
                Principal's <br/> Monthly Income
              </div>
            </div>
          </div>
          <div className='ui card'>
            <div className='content' style={{textAlign: 'center'}}>
              <div className='ui form'>
                <div className="ui right left icon input">
                  <i className="dollar icon" style={{opacity: 'inherit', fontSize: '1.2em'}}></i>
                  <input id='spouseIncome' onChange={this.onChangeSpouseIncome.bind(this)} style={styles.editableSpouse} type="text" value={spouseIncome} />
                </div>
              </div>
            </div>
            <div className='ui segment' style={styles.imageAlign}>
              <img className='ui centered medium image' src='/images/icon_Vector-Smart-Object.png' style={styles.icon}/>
            </div>
            <div className='ui bottom attached buttons'>
              <div className='ui button' style={styles.title}>
                Spouse's <br/> Monthly Income
              </div>
            </div>
          </div>
          <div className='ui card'>
            <div className='content' style={{textAlign: 'center'}}>
              <h3> {totalIncome}</h3>
            </div>
            <div className='ui segment' style={styles.imageAlign}>
              <img className='ui centered medium image' src='/images/icon_Total-household -Monthly-Income.png' style={styles.icon}/>
            </div>
            <div className='ui bottom attached buttons'>
              <div className='ui button' style={styles.title}>
                Total household <br/> Monthly Income
              </div>
            </div>
          </div>
          <div className='ui card'>
            <div className='content' style={{textAlign: 'center'}}>
              <h3> {totalExpense}</h3>
            </div>
            <div className='ui segment' style={styles.imageAlign}>
              <img className='ui centered medium image' src='/images/icon_Total-Monthly-Expense.png' style={styles.icon}/>
            </div>
            <div className='ui bottom attached buttons'>
              <div className='ui button' style={styles.title}>
                Total <br/> Monthly Expense
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  title: {
    background: '#FFBE04',
    boxShadow: 'none',
    color: '#B00414',
    cursor: 'default',
    flex: 'inherit',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 13,
    lineHeight: 'inherit',
    padding: '5px 10px',
    width: '100%',
  },
  editableSpouse: {
    fontSize: '1.2em',
    fontWeight: 'bold',
    padding: '5px 0px'
  },
  container: {
    marginTop: 20
  },
  icon: {
    width: 50,
    height: 50  
  },
  imageAlign:  {
    background: '#F5F2ED',
    border: 0,
    boxShadow: 'none',
    paddingTop: 0,
    margin: 0
  }
}
