import React from 'react';
import { 
  loadAssets
} from '../actions/assets';

const SubHeader = ({ dispatch }) => {

  var loadAssets = () => {
    // console.log(this.props);
    // dispatch(loadAssets());
  }


  return (
    <div className="ui top segment menu" style={styles.subheader}>
      <div className='ui container'>
        Personal Net Worth Statement for Borrower 1
        <div className="right menu">
            <button className="ui red basic light button" 
              style={{color: '#FFF'}} onClick={loadAssets}>Refresh Assets & Liabilities</button>
            <button className="ui red basic light button" 
              style={{color: '#FFF'}}>Print</button>
          </div>
      </div>
    </div>
  )
}

export default SubHeader;

const styles = {
  subheader: {
    background: '#B00414',
    color: '#FFF',
    fontWeight: 'bold',
    marginTop: 0,
    paddingLeft: 30,
    paddingRight: 30
  }
}
