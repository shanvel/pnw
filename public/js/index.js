import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import App from './containers/App';
import pnwApp from './reducers';

const loggerMiddleware = createLogger();
const store = createStore(pnwApp, applyMiddleware(thunkMiddleware, loggerMiddleware));

console.log(store)

const router = (
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

render(router, document.getElementById('app'));
