import axios from 'axios';
// import _ from 'underscore';
import _ from 'lodash';
import CONFIG from '../config';
import { showTotalAssets } from './summary';

export function toggleForm() {
  return {
    type: 'TOGGLE_CREATE_ASSET_FORM'
  }
}

function showAllAssets(allAssets) {
  return {
    type: 'SHOW_ALL_ASSETS',
    allAssets
  }
}

export function showAssetType(assetType) {
  return {
    type: 'SHOW_ASSET_TYPE',
    assetType
  }
}

function deleteEvent(assetId) {
  return {
    type: 'DELETE_ASSET',
    assetId
  }
}

export function loadAssets() {
  return (dispatch, getState) => {
    axios.get(
      `${CONFIG.DOMAIN_BASE_URL}/${CONFIG.END_POINT.ALL_ASSETS_GET}`
    )
    .then(response => {
      if (response.status == 200) {
        // dispatch();
      }
      let assets = response.data;

      let filteredResponse = _.map(assets, asset => {
        let filtered = _.pickBy(asset, _.identity);
        return filtered;
      })

      console.log(filteredResponse)
      // let ans = _.pickBy({ a: null, b: 1, c: undefined }, _.identity);

      // console.log(ans);
      console.log('GOT RESONSE', response.headers);
      dispatch(showAllAssets(filteredResponse));

      let sumOfEstimatedBalance = _.sumBy(filteredResponse, 'estimatedBalance');

      // update summary section ...
      dispatch(showTotalAssets(sumOfEstimatedBalance));
    })
    .catch(error => {
      console.log("FETCH CAUGHT error----")
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else {
        console.log(error.message);
      }
      console.log(error.config);
    })
    
    // dispatch(showAllAssets(result));
  }
}

export function toggleCreateAssetsForm() {
  return (dispatch, getState) => {
    dispatch(toggleForm());
  }
}

export function deleteAsset(assetId) {
  console.log('ON DELTE CALLED', assetId);
  return (dispatch, getState) => {
    axios.delete(
      `${CONFIG.DOMAIN_BASE_URL}/${CONFIG.END_POINT.ASSET_DELETE}/${assetId}`
    )
    .then(response => {
      console.log(response);
      dispatch(deleteEvent(assetId));
    })
    .catch(error => {
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else {
        console.log(error.message);
      }
      console.log(error.config);
    })
    
  }
}


export function editAsset(assetId, assetType) {
  console.log('ON EDIT CALLED', assetId, assetType);
  return (dispatch, getState) => {
    dispatch(toggleForm());
  }
}