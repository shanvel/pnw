import axios from 'axios';

export const SHOW_TOTAL_ASSETS = 'SHOW_TOTAL_ASSETS';
export function showTotalAssets(total) {
  return {
    type: SHOW_TOTAL_ASSETS,
    totalAssets: total
  }
}

export const SHOW_TOTAL_LIABILITIES = 'SHOW_TOTAL_LIABILITIES';
function showTotalLiabilities(total) {
  return {
    type: SHOW_TOTAL_LIABILITIES,
    totalLiabilities: total
  }
}

export const SHOW_TOTAL_PAYMENTS = 'SHOW_TOTAL_PAYMENTS';
function showTotalPayments(total) {
  return {
    type: SHOW_TOTAL_PAYMENTS,
    totalPayments: total
  }
}

export const SHOW_PRINCIPAL_INCOME = 'SHOW_PRINCIPAL_INCOME';
function showPrincipalIncome(income) {
  return {
    type: SHOW_PRINCIPAL_INCOME,
    principalIncome: income
  }
}

export const SHOW_SPOUSE_INCOME = 'SHOW_SPOUSE_INCOME';
function showSpouseIncome(income) {
  return {
    type: SHOW_SPOUSE_INCOME,
    spouseIncome: income
  }
}

export const SHOW_TOTAL_EXPENSE = 'SHOW_TOTAL_EXPENSE';
function showTotalExpense(expense) {
  return {
    type: SHOW_TOTAL_EXPENSE,
    totalExpense: expense
  }
}

export function fetchSummary() {
  return function(dispatch, getState) {
    // getState() --> prints Current application store

    axios.get("data.json")
    .then((response) => {
      console.log('RESPONSE ', response);
      let { 
        // assets, 
        liabilities, 
        total_payment, 
        principal_income, 
        spouse_income,
        total_expense,
      } = response.data.summary;
      // dispatch(showTotalAssets(assets));
      dispatch(showTotalLiabilities(liabilities));
      dispatch(showTotalPayments(total_payment));
      dispatch(showPrincipalIncome(principal_income));
      dispatch(showSpouseIncome(spouse_income));
      dispatch(showTotalExpense(total_expense));
    })
    .catch((err) => {
      console.log(err);
      return err;
    })
  }
}
