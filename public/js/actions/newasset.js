import lodash from 'lodash';
import axios from 'axios';
import CONFIG from '../config';

import { showAssetType, toggleForm } from './assets';

export const SHOW_CREATE_BANK_ASSET_TYPE = 'SHOW_CREATE_BANK_ASSET_TYPE';
export const SHOW_CREATE_MOTOR_VEHICLE_TYPE = 'SHOW_CREATE_MOTOR_VEHICLE_TYPE';

const BANK_ACCOUNT = 'bank_account';
const MOTOR_VEHICLE = 'motor_vehicle';
const NOT_IN_SCOPE = 'not_in_scope';

// function showAssetType(newAssetType) {
//   return {
//     type: newAssetType,
//     newAssetType: newAssetType
//   }
// }

function addAsset(asset) {
  return {
    type: 'ADD_ASSET',
    asset
  }
}

export function showNewAssetForm(assetType) {
  return (dispatch, getState) => {
    switch(assetType) {
      case BANK_ACCOUNT:
        return dispatch(showAssetType(SHOW_CREATE_BANK_ASSET_TYPE));
      case MOTOR_VEHICLE:
        return dispatch(showAssetType(SHOW_CREATE_MOTOR_VEHICLE_TYPE));
      default:
        return dispatch(showAssetType(NOT_IN_SCOPE));
    }
  }
}

export function saveNewAsset(asset) {
  console.log('TO SAVE -------', asset);

  return (dispatch, getState) => {
    let params = asset;
    params.customerId = CONFIG.CUSTOMER_ID

    axios.post(
      `${CONFIG.DOMAIN_BASE_URL}/${CONFIG.END_POINT.NEW_ASSET_POST}`,
      params
    )
    .then(response => {
      if (response.status == 200) {
        let data = response.data;
        let filteredData = _.pickBy(data, _.identity);

        dispatch(addAsset(filteredData));
        dispatch(toggleForm());
      }
      console.log(response);
    })
    .catch(error => {
      console.log("CAUGHT error----")
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else {
        console.log(error.message);
      }
      console.log(error.config);
    })
  }
}
