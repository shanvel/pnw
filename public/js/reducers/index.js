import { combineReducers } from 'redux';
import asset from './asset';
import summary from './summary';
import newAssetType from './newasset';

const pnwApp = combineReducers({
    root: asset,
    summary,
    newAssetType
  }
);

export default pnwApp;
