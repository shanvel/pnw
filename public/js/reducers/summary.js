import {
  SHOW_TOTAL_ASSETS,
  SHOW_TOTAL_LIABILITIES,
  SHOW_TOTAL_PAYMENTS,
  SHOW_PRINCIPAL_INCOME,
  SHOW_SPOUSE_INCOME,
  SHOW_TOTAL_EXPENSE
} from '../actions/summary';

const summary = (state = {}, action) => {
  switch(action.type) {
    case SHOW_TOTAL_ASSETS:
      return {
        ...state,
        ...{
          totalAssets: action.totalAssets
        }
      }
    case SHOW_TOTAL_LIABILITIES:
      return {
        ...state,
        ...{
          totalLiabilities: action.totalLiabilities
        }
      }
    case SHOW_TOTAL_PAYMENTS:
      return {
        ...state,
        ...{
          totalPayments: action.totalPayments
        }
      }
    case SHOW_PRINCIPAL_INCOME:
      return {
        ...state,
        ...{
          principalIncome: action.principalIncome
        }
      }
    case SHOW_SPOUSE_INCOME:
      return {
        ...state,
        ...{
          spouseIncome: action.spouseIncome
        }
      }
    case SHOW_TOTAL_EXPENSE:
      return {
        ...state,
        ...{
          totalExpense: action.totalExpense
        }
      }
    default:
      return state;
  }
}

export default summary;
