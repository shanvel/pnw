import _ from 'lodash';

let initial = {
  visible: false,
  assets: [],
  assetType: {}
}

const asset = (state = initial, action) => {
  switch (action.type) {
    case 'SHOW_ALL_ASSETS':
      const assets = [...state.assets, ...action.allAssets];
      return {
        ...state,
        assets
      }
    case 'TOGGLE_CREATE_ASSET_FORM':
      return {
        ...state,
        ...{
          visible: !state.visible
        }
      }
    case 'SHOW_ASSET_TYPE':
      return {
        ...state,
        ...{
          assetType: action.assetType
        }
      }
    case 'ADD_ASSET':

      console.log("ADD ASSET---", state)
      return {
        ...state,
        assets: 
        [
          ...state.assets,
          action.asset
        ]
      }
    case 'DELETE_ASSET':
      let allAssets = state.assets;
      let filteredAssets = _.reject(allAssets, asset => {
        return (asset.id === action.assetId);
      })

      return {
        ...state,
        assets: filteredAssets
      }

    default:
      return state;
  }
}

export default asset;
