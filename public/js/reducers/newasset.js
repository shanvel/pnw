import {
  SHOW_CREATE_BANK_ASSET_TYPE,
  SHOW_CREATE_MOTOR_VEHICLE_TYPE
} from '../actions/newasset';

const newasset = (state = "", action) => {
  switch(action.type) {
    case SHOW_CREATE_BANK_ASSET_TYPE:
      return action.newAssetType;
    case SHOW_CREATE_MOTOR_VEHICLE_TYPE:
      return action.newAssetType;
    default:
      return "NOT_IN_SCOPE";
  }
}

export default newasset;
