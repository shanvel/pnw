export default {
  CLIENT_ID: '',
  CUSTOMER_ID: 434434,
  DOMAIN_BASE_URL: 'http://172.24.30.100:8080',
  END_POINT: {
    'NEW_ASSET_POST': 'v1/score/biz/client/345/pnw/assets',
    'NEW_ASSET_PUT': 'v1/score/biz/client/345/pnw/assets',
    'ALL_ASSETS_GET': 'v1/score/biz/client/345/pnw/assets',
    'ASSET_DELETE': 'v1/score/biz/client/345/pnw/assets'
  },
  ICON: {
    'BANK_ACCOUNT': 'icon_BankAccount.png',
    'MOTOR_VEHICLE': 'icon_Moter-VechicleCar'
  }
}
