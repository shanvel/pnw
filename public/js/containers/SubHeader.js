import React, { Component } from 'react';
import { connect } from 'react-redux';
import SubHeader from '../components/SubHeader';

const mapStateToProps = state => {
  return state
}

export default connect(mapStateToProps)(SubHeader);
