import React, { Component } from 'react';
import { connect } from 'react-redux';
import Assets from '../components/Assets';

const mapStateToProps = state => {
  return {
    assets: state.root.assets,
    visible: state.root.visible
  }
}

export default connect(mapStateToProps)(Assets);
