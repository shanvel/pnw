import React, { Component } from 'react';
import { connect } from 'react-redux';
import Summary from './Summary';
import Assets from './Assets';
import Liabilities from '../components/Liabilities';
import Income from '../components/Income';
import Expense from '../components/Expense';

const Content = () => (
	<div>
		<Summary />
		<Assets />
		<Liabilities />
		<Income />
		<Expense />
	</div>
)

export default Content;
