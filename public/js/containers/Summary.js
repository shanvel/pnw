import React, { Component } from 'react';
import { connect } from 'react-redux';
import Summary from '../components/Summary';

const mapStateToProps = state => {
  return {
    /** Remove summary key 
        & add 'liability, income, expense' keys, 
        after all the other sections are dynamically fetched from API Services...
    */
    summary: state.summary, 
    assets: state.root.assets
  }
  // return state;  // to return the entire application state...
}

export default connect(mapStateToProps)(Summary);
