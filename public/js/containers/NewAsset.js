import React, { Component } from 'react';
import { connect } from 'react-redux';
import NewAsset from '../components/NewAsset';

const mapStateToProps = state => {
  return state
}

export default connect(mapStateToProps)(NewAsset);
